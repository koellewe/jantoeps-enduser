package com.sps.jvr;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sps.jvr.utils.JanTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainScreen extends Activity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        JanTools.setContext(this);

        Calendar calendar = Calendar.getInstance();
        minutesOfDay = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
        if (minutesOfDay >= 845 || calendar.get(Calendar.DAY_OF_WEEK) == 1 || calendar.get(Calendar.DAY_OF_WEEK) == 7) {
            gotoActivity(MainScreenNaSkool.class);
        } else {

            if (JanTools.checkConnection()) {
                int year = calendar.get(Calendar.YEAR) - (Integer.parseInt(read("com.sps.jvr.graad")) + 6);      //2015 - (10 + 6) = 1999
                if (JanTools.read("graad", "0"))
                    year = calendar.get(Calendar.YEAR) - 38; //avg onnie ouderdom
                if (!JanTools.read("geboorteJaar", "0")){
                    year = Integer.parseInt(JanTools.read("geboorteJaar"));
                }

                AdView mAdView = (AdView) findViewById(R.id.adView);

                AdRequest.Builder builder = new AdRequest.Builder()
                        .setBirthday(new GregorianCalendar(year, 1, 1).getTime());
                        //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

                if (!JanTools.read("geslag", "0")){
                    switch (JanTools.read("geslag")){
                        case "Vroulik":
                            builder.setGender(AdRequest.GENDER_FEMALE);
                            break;
                        case "Manlik":
                            builder.setGender(AdRequest.GENDER_MALE);
                            break;
                        case "Ander":
                            builder.setGender(AdRequest.GENDER_UNKNOWN);
                            break;
                    }
                }

                mAdView.loadAd(builder.build());
            } else {
                findViewById(R.id.adView).setVisibility(View.GONE);              //Remove adview
                findViewById(R.id.imgJVR).setVisibility(View.VISIBLE);        //Visibilize JVRlogo

                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.setMargins(0, Math.round(JanTools.convertPixelsToDp(20)), 0, Math.round(JanTools.convertPixelsToDp(100)));
                p.addRule(RelativeLayout.BELOW, R.id.imgJVR);
                findViewById(R.id.btnVolleRooster).setLayoutParams(p);           //set btn below JVRlogo
            }

            updateEverything();

            if ((read("com.sps.jvr.naam").equals("0") || JanTools.read("geslag", "0")) && !JanTools.readBool("dnsLeeProfiel")) {
                ((TextView) findViewById(R.id.txtHallo)).setText(R.string.jounaam_bold);
                final CheckBox chk = new CheckBox(this);
                chk.setText("Moet nie hierdie weer wys nie.");

                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Leë Profiel")
                        .setMessage("Lyk my jy het nog nie jou profiel opgestel nie. Wanneer wil jy dit doen?")
                        .setView(chk)
                        .setPositiveButton("Nou", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (chk.isChecked()){
                                    JanTools.write("dnsLeeProfiel", "true");
                                }
                                gotoActivity(Profile.class);
                            }
                        })
                        .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (chk.isChecked()){
                                    JanTools.write("dnsLeeProfiel", "true");
                                }
                            }
                        })
                        .show();
            } else if (JanTools.read("naam", "0")) {
                ((TextView) findViewById(R.id.txtHallo)).setText(R.string.jounaam_bold);
            }else{
                ((TextView) findViewById(R.id.txtHallo)).setText("Hallo, " + read("com.sps.jvr.naam") + "!");
            }

        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    //public variables
    public int minutesOfDay = 0;
    public Handler handler = new Handler();
    public Handler spamHandler = new Handler();
    public boolean spamHandlerSet = false;
    public boolean spamTimer = false;
    public boolean handlerSet = false;

    public static final String url_get_afks = "http://apps.rauten.co.za/jantoeps/get_all_afkondigings.php";

    //--------------------------------------onClicks

    public void onRefreshClick(View view) {
        if (!spamHandlerSet) {
            spamHandlerSet = true;
            spamHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    spamTimer = false;      //ended
                    spamHandlerSet = false;
                }
            }, 3000);
        }
        if (spam() == true) {    //net vir jou, Dave.
            killThatMotherFuckerDead();
        } else {
            updateEverything();
            makeToast("Vervris.", "short");
            spamTimer = true;   //started
        }
    }

    public void onHalloClick(View view) {
        gotoActivity(Profile.class);
    }

    public void onSettingsClick(View view) {
        ((ImageView) view).setImageResource(R.drawable.cogclicked);
        gotoActivity(Settings.class);
    }

    public void onVolleRoosterClick(View view) {
        Calendar c = Calendar.getInstance();
        if (!read("com.sps.jvr.switchVakke").equals("false") && !read("com.sps.jvr.switchVakke").equals("0")) {
            write("com.sps.jvr.tempSienDagRooster", read("com.sps.jvr.switchVakke"));
        } else {
            write("com.sps.jvr.tempSienDagRooster", JanTools.dayToDagLank(c.get(Calendar.DAY_OF_WEEK)));
        }
        gotoActivity(VolleRooster.class);
    }

    //--------------------------------------Other Methods

    @SuppressLint("SetTextI18n")
    public void updateEverything() {

        if (JanTools.readBool("com.sps.jvr.sync")) {
            new GetAfkondigings().execute();
        }

        //checkWeirdTye();

        if (read("com.sps.jvr.vakansie", "true")) {
            gotoActivity(Vakansie.class);
        } else {
            if (read("com.sps.jvr.eksamen", "true")) {
                gotoActivity(Eksamen.class);
            } else {

                Calendar calendar = Calendar.getInstance();
                minutesOfDay = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);

                if (minutesOfDay >= 845) {
                    gotoActivity(MainScreenNaSkool.class);
                }

                if (!read("com.sps.jvr.maandagTye").equals("false")
                        && !read("com.sps.jvr.maandagTye").equals(calendar.get(Calendar.DAY_OF_WEEK) + "")) {
                    write("com.sps.jvr.maandagTye", "false");
                }
                if (!read("com.sps.jvr.switchVakke").equals("false")) {
                    if (!read("com.sps.jvr.switchVakkeDag").equals(JanTools.dayToDagLank(calendar.get(Calendar.DAY_OF_WEEK)))) {
                        write("com.sps.jvr.switchVakkeDag", "false");
                        write("com.sps.jvr.switchVakke", "false");
                    }
                }

                int Reg;
                int P1;
                int P2;
                int P3;
                int Pouse1;
                int P4;
                int P5;
                int P6;
                int Pouse2;
                int P7;

                if ((calendar.get(Calendar.DAY_OF_WEEK) == 2) || !read("com.sps.jvr.maandagTye").equals("false")) {
                    Reg = 520;
                    P1 = 555;
                    P2 = 590;
                    P3 = 625;
                    Pouse1 = 645;
                    P4 = 680;
                    P5 = 715;
                    P6 = 750;
                    Pouse2 = 775;
                    P7 = 810;
                } else {
                    Reg = 480;
                    P1 = 520;
                    P2 = 560;
                    P3 = 600;
                    Pouse1 = 620;
                    P4 = 660;
                    P5 = 700;
                    P6 = 740;
                    Pouse2 = 765;
                    P7 = 805;
                }

                String[] vakke = new String[8];

                String dag;
                if (!read("com.sps.jvr.switchVakke").equals("false") && !read("com.sps.jvr.switchVakke").equals("0")) {
                    dag = read("com.sps.jvr.switchVakke");
                } else {
                    dag = JanTools.dayToDagLank(calendar.get(Calendar.DAY_OF_WEEK));
                }
                for (int i = 0; i < 8; i++) {
                    vakke[i] = read("com.sps.jvr." + dag.toLowerCase() + "P" + (i + 1));
                }


                int[] p = {1, 2, 3, 4, 5, 6, 7, 8};

                if (read("com.sps.jvr.volgordeDag", calendar.get(Calendar.DAY_OF_WEEK) + "") && !read("com.sps.jvr.volgordeDag", "false") && !read("com.sps.jvr.volgordeDag", "0")) {
                    if (!read("com.sps.jvr.volgorde", "12345678")) {
                        for (int i = 0; i < 8; i++) {
                            p[i] = Integer.parseInt(read("com.sps.jvr.volgorde").substring(i, i + 1));
                        }
                        String[] tempV = new String[8];
                        System.arraycopy(vakke, 0, tempV, 0, 8);
                        for (int i = 0; i < 8; i++) {
                            vakke[i] = tempV[p[i] - 1];
                        }
                    }
                } else {
                    write("com.sps.jvr.volgordeDag", "false");
                }

                TextView txtEersteKlok = (TextView) findViewById(R.id.txtEersteKlok);
                txtEersteKlok.setVisibility(View.GONE);

                TextView txtCurrentPeriode = (TextView) findViewById(R.id.txtCurrentPeriode);
                TextView txtNextPeriode = (TextView) findViewById(R.id.txtNextPeriode);
                TextView txtMinuteOor = (TextView) findViewById(R.id.txtMinuteOor);

                if (minutesOfDay < 460) {
                    txtCurrentPeriode.setText("Dit is nou voor skool.");
                    if (calendar.get(Calendar.DAY_OF_WEEK) == 2) {
                        txtNextPeriode.setText("Saal");
                    } else {
                        txtNextPeriode.setText("Register");
                    }

                    int minsVoor = 460 - minutesOfDay;
                    int ure = 0;

                    while (minsVoor >= 60) {
                        minsVoor -= 60;
                        ure++;
                    }

                    String text;
                    if (ure == 1) text = "meer as 'n uur";
                    else if (ure > 1) text = "meer as " + ure + " ure";
                    else text = minsVoor + " minute";

                    if (ure == 0) setEersteKlok(460 - minutesOfDay - 5);

                    txtMinuteOor.setText(text);
                    write("com.sps.jvr.currentPeriode", "-1");
                } else if (minutesOfDay < Reg) {
                    if (calendar.get(Calendar.DAY_OF_WEEK) == 2) {
                        txtCurrentPeriode.setText("Dit is nou saal");
                    } else {
                        txtCurrentPeriode.setText("Dit is nou register");
                    }
                    txtNextPeriode.setText("Periode " + p[0] + " " + vakke[0]);
                    txtMinuteOor.setText(Reg - minutesOfDay + " minute");
                    JanTools.write("currentPeriode", "0");
                } else if (minutesOfDay < P1) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[0] + " " + vakke[0]);
                    txtNextPeriode.setText("Periode " + p[1] + " " + vakke[1]);
                    txtMinuteOor.setText(P1 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "1");
                } else if (minutesOfDay < P2) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[1] + " " + vakke[1]);
                    txtNextPeriode.setText("Periode " + p[2] + " " + vakke[2]);
                    txtMinuteOor.setText(P2 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "2");
                } else if (minutesOfDay < P3) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[2] + " " + vakke[2]);
                    txtNextPeriode.setText("1ste Pouse");
                    txtMinuteOor.setText(P3 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "3");
                } else if (minutesOfDay < Pouse1) {
                    txtCurrentPeriode.setText("Dit is nou 1ste Pouse");
                    txtNextPeriode.setText("Periode " + p[3] + " " + vakke[3]);
                    txtMinuteOor.setText(Pouse1 - minutesOfDay + " minute");
                    setEersteKlok(Pouse1 - minutesOfDay - 5);
                    write("com.sps.jvr.currentPeriode", "4");
                    checkLaatBus();
                } else if (minutesOfDay < P4) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[3] + " " + vakke[3]);
                    txtNextPeriode.setText("Periode " + p[4] + " " + vakke[4]);
                    txtMinuteOor.setText(P4 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "5");
                } else if (minutesOfDay < P5) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[4] + " " + vakke[4]);
                    txtNextPeriode.setText("Periode " + p[5] + " " + vakke[5]);
                    txtMinuteOor.setText(P5 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "6");
                } else if (minutesOfDay < P6) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[5] + " " + vakke[5]);
                    txtNextPeriode.setText("2de Pouse");
                    txtMinuteOor.setText(P6 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "7");
                } else if (minutesOfDay < Pouse2) {
                    txtCurrentPeriode.setText("Dit is nou 2de Pouse");
                    txtNextPeriode.setText("Periode " + p[6] + " " + vakke[6]);
                    txtMinuteOor.setText(Pouse2 - minutesOfDay + " minute");
                    setEersteKlok(Pouse2 - minutesOfDay - 5);
                    write("com.sps.jvr.currentPeriode", "8");
                } else if (minutesOfDay < P7) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[6] + " " + vakke[6]);
                    txtNextPeriode.setText("Periode " + p[7] + " " + vakke[7]);
                    txtMinuteOor.setText(P7 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "9");
                } else if (minutesOfDay < 845) {
                    txtCurrentPeriode.setText("Dit is nou Periode " + p[7] + " " + vakke[7]);
                    txtMinuteOor.setText(845 - minutesOfDay + " minute");
                    write("com.sps.jvr.currentPeriode", "10");
                    setNaSkool((calendar.get(Calendar.DAY_OF_WEEK)));
                }

                int pos = 0;
                String txtMinute = txtMinuteOor.getText().toString();
                while (!txtMinute.substring(pos, pos + 1).equals(" ")) {
                    pos++;
                }
                if (txtMinute.substring(0, pos).equals("1")) {
                    txtMinuteOor.setText("1 minuut");
                }

                if (read("com.sps.jvr.vibreer").equals("true")) {

                    //Schedule vibrate
                    Runnable startVibrate = new Runnable() {
                        @Override
                        public void run() {
                            vibrate();
                            handlerSet = false;
                        }
                    };

                    int minuteOor = Integer.parseInt(txtMinute.substring(0, pos));
                    if (minuteOor > 2) {
                        if (!handlerSet) {
                            handler.postDelayed(startVibrate, (minuteOor - 5) * 60 * 1000);
                            handlerSet = true;
                        }
                    }
                }
            }
        }
    }

    public boolean spam() {
        return spamTimer;
    }

    public void killThatMotherFuckerDead() {
        //doesn't do anything but that's the point...
    }

    private void setGebeurtenisse() {
        ((TextView) findViewById(R.id.txtGebeurTitel)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.imgPyl)).setVisibility(View.VISIBLE);
        TextView txt = (TextView) findViewById(R.id.txtGebeur);

        //nuwe updates hier ook
        String display = "";
        if (JanTools.readBool("com.sps.jvr.weird")) {
            display += "• Tye is nie vandag normaal nie.\n";    //LW: as jy '???' sien, vervang dit met • (ALT+0149)
        }

        if (!JanTools.read("com.sps.jvr.afkondigings", "false")) {

            String afkondRaw = JanTools.read("afkondigings");

            int prevI = 0;
            for (int i = 0; i < afkondRaw.length(); i++) {   //count semis
                if (afkondRaw.substring(i, i + 1).equals(";")) {
                    display += "• " + afkondRaw.substring(prevI, i) + "\n";
                    prevI = i + 1;
                }
            }
        }

        if (checkNuweWeergawe(MainScreen.this)) {
            display += "• Daar is 'n nuwe weergawe van die toeps beskikbaar.\n";
        }
        //more ifs with \n

        txt.setText(display);
    }


    private ProgressDialog dialog;
    class GetAfkondigings extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainScreen.this);
            dialog.setMessage("Verkry tans afkondigings...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... v) {

            try {

                HttpURLConnection httpCon = (HttpURLConnection) new URL(url_get_afks).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    //System.out.println("RESPONSE = " + sb.toString());

                    return new JSONObject(sb.toString());

                } else {
                    JanTools.makeToast("HTTP Flater");
                }

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(final JSONObject json) {
            //super.onPostExecute(s);
            dialog.dismiss();

            if (json == null) {
                JanTools.makeToast("Kon nie afkondigings kry nie.");
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        postCustom(json);
                    }
                });
            }
        }

        private void postCustom(JSONObject json){

            try {
                JSONArray jarr = json.getJSONArray("afkondigings");
                StringBuilder afkRaw = new StringBuilder();

                for (int i = 0; i < jarr.length(); i++){
                    afkRaw.append(((JSONObject) jarr.get(i)).getString("afkondiging"));
                    afkRaw.append(";");//seperator
                }

                JanTools.write("afkondigings", afkRaw.toString());

                setGebeurtenisse();

            } catch (JSONException e) {
                e.printStackTrace();
                JanTools.makeToast("Kon nie afkondigings verwerk nie.");
            }


        }
    }

    public void vibrate() {
        if (read("com.sps.jvr.vibreer").equals("true")) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] pattern = {
                    0,
                    400,
                    50,
                    100,
                    50,
                    100,
                    50,
                    200,
                    50,
                    250,
                    500,
                    100,
                    75,
                    500,
            };
            v.vibrate(pattern, -1);
        }
    }

    public void checkLaatBus() {
        if (!read("com.sps.jvr.naSkool" + vandagKort(), "[Niks]")) {
            if (read("com.sps.jvr.laatBus", "true")) {
                new AlertDialog.Builder(this)
                        .setTitle("Laat Bus")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Jy het " + read("com.sps.jvr.naSkool" + vandagKort()) + " na skool. Maak seker jou naam is op die laatbuslys indien nodig.")
                        .setNeutralButton("Regso", null)
                        .show();
            }
        }
    }

    public void setNaSkool(int dag) {
        TextView txtNextPeriode = (TextView) findViewById(R.id.txtNextPeriode);

        if (JanTools.read("naSkool"+JanTools.dayToDagKort(dag), "[Niks]")){
            if (dag == 6){
                txtNextPeriode.setText("NAWEEK");
            }else{
                txtNextPeriode.setText("Skool Verby");
            }
        }else{
            txtNextPeriode.setText(JanTools.read("naSkool"+JanTools.dayToDagKort(dag)));
        }
    }

    public void setEersteKlok(int mins) {
        TextView txtEersteKlok = (TextView) findViewById(R.id.txtEersteKlok);
        txtEersteKlok.setVisibility(View.VISIBLE);
        if (mins == 1) {
            txtEersteKlok.setText("(1 minuut voor eerste klok lui)");
        } else if (mins <= 0) {
            txtEersteKlok.setText("Eerste klok het al gelui.");
        } else { //mins > 1
            txtEersteKlok.setText("(" + mins + " minute voor eerste klok lui)");
        }
    }

    public String vandagKort() {
        Calendar c = Calendar.getInstance();
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 2:
                return "Ma";
            case 3:
                return "Di";
            case 4:
                return "Wo";
            case 5:
                return "Do";
            case 6:
                return "Vr";
        }
        return "";
    }

    static public boolean checkNuweWeergawe(Activity act) {
        if (!JanTools.read("com.sps.jvr.version", "false")) {
            int latestVersion = Integer.parseInt(JanTools.read("version"));
            int currentVersion = 0;
            try {
                PackageInfo pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
                currentVersion = pInfo.versionCode;
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (currentVersion == 0) {
                JanTools.makeToast("Weergawe flater.");
            } else {
                if (currentVersion < latestVersion) {
                    return true;
                }
            }
        }
        return false;
    }

    //--------------------------------------Tool Methods

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void write(String key, String val) {
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key) {
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String in, String cmp) {
        return read(in).equals(cmp);
    }

    public void makeToast(String say, String length) {
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        } else {
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Stone Pillar Studios",
                Uri.parse("http://rauten.ddns.net/sps"),
                Uri.parse("android-app://com.sps.jvr/http/host")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Stone Pillar Studios",
                Uri.parse("http://rauten.ddns.net/sps"),
                Uri.parse("android-app://com.sps.jvr/http/host")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
