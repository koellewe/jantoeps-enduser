package com.sps.jvr;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class SwitchPeriodes extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_periodes);
        setTxts();
    }

    //--------------------------------------------Other Methods

    public void setTxts(){
        Calendar c = Calendar.getInstance();
        int[] volgorde = new int[8];
        if (!read("com.sps.jvr.volgordeDag", "false") && !read("com.sps.jvr.volgordeDag", "0")) {
            for (int i = 0; i < 8; i++){
                volgorde[i] = Integer.parseInt(read("com.sps.jvr.volgorde").substring(i, i + 1));
            }
            for (int i = 0; i < 8; i++) {
                int Id = getResources().getIdentifier("txt" + (i + 1), "id", getPackageName());
                ((TextView) findViewById(Id)).setText(String.valueOf(volgorde[i]));
            }
        }else{
            write("com.sps.jvr.volgordeDag", "false");
        }
    }

    //--------------------------------------------OnClicks

    public void onDownClick(View view){
        int level = getLevel(view);
        String IdString = "txt" + level;
        int Id = getResources().getIdentifier(IdString, "id", getPackageName());
        TextView txt = (TextView) findViewById(Id);
        int setTo = Integer.parseInt(txt.getText().toString()) - 1;
        if (setTo == 0){
            setTo = 8;
        }
        txt.setText(setTo + "");
    }

    public void onUpClick(View view){
        int level = getLevel(view);
        String IdString = "txt" + level;
        int Id = getResources().getIdentifier(IdString, "id", getPackageName());
        TextView txt = (TextView) findViewById(Id);
        int setTo = Integer.parseInt(txt.getText().toString()) + 1;
        if (setTo == 9){
            setTo = 1;
        }
        txt.setText(setTo + "");
    }

    public void onKlaarClick(View view){
        int[] periodeNommers = new int[8];
        for (int i = 0; i < 8; i++){
            int Id = getResources().getIdentifier("txt" + (i + 1), "id", getPackageName());
            periodeNommers[i] = Integer.parseInt(((TextView) findViewById(Id)).getText().toString());
        }
        boolean duplikaat = false;
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                if (periodeNommers[i] == periodeNommers[j] && i != j){
                    duplikaat = true;
                }
            }
        }
        if (!duplikaat) {
            Calendar c = Calendar.getInstance();
            write("com.sps.jvr.volgorde", "");
            write("com.sps.jvr.volgordeDag", c.get(Calendar.DAY_OF_WEEK ) + "");
            for (int i = 0; i < 8; i++) {
                write("com.sps.jvr.volgorde", read("com.sps.jvr.volgorde") + periodeNommers[i]);
            }
            if (read("com.sps.jvr.volgorde", "12345678")){
                write("com.sps.jvr.volgordeDag", "false");
            }
            makeToast("Stellings gestoor.", "short");
            gotoActivity(Settings.class);
        }else{
            makeToast("Duplikaat gevind. Maak reg voordat jy voortgaan.", "short");
        }
    }

    public void onHerstelClick(View view){
        for (int i = 0; i < 8; i++){
            int Id = getResources().getIdentifier("txt" + (i + 1), "id", getPackageName());
            ((TextView) findViewById(Id)).setText((i + 1) + "");
        }
    }

    public void onBackPressed() {
        onKlaarClick(findViewById(R.id.btnStoor));
    }

    //---------------------------------------------Eie Methods

    public int getLevel(View view){
        String s = view.getResources().getResourceName(view.getId());
        int len = s.length();
        return Integer.parseInt(s.substring(len - 1));
    }

    //--------------------------------------------Tool Methods

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String read, String cmp){
        return read(read).equals(cmp);
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }
}
