package com.sps.jvr;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sps.jvr.utils.JanTools;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;

public class VolleRooster extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volle_rooster);
        JanTools.setContext(this);

        dagView = read("com.sps.jvr.tempSienDagRooster");
        if (dagView.equals("Saterdag") || dagView.equals("Sondag")){
            dagView = "Maandag";
        }
        Calendar c = Calendar.getInstance();
        int minute = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);
        if (minute >= 845){
            ImageButton btn = (ImageButton) findViewById(R.id.btnDagOp);
            btn.performClick();
        }else{
            setDagText(dagView);
            setTye(dagView);
            setVakke(dagView);
            setVolgorde(read("com.sps.jvr.volgorde"));
            setBold(read("com.sps.jvr.currentPeriode"));
            setGrys();
        }
    }

    //public variables
    public String dagView = "Maandag";
    //end of public variables

    public void setTye(String dag){
        TextView txtTydP1 = (TextView) findViewById(R.id.txtTydP1);
        TextView txtTydP2 = (TextView) findViewById(R.id.txtTydP2);
        TextView txtTydP3 = (TextView) findViewById(R.id.txtTydP3);
        TextView txtTydPouse1 = (TextView) findViewById(R.id.txtTydPouse1);
        TextView txtTydP4 = (TextView) findViewById(R.id.txtTydP4);
        TextView txtTydP5 = (TextView) findViewById(R.id.txtTydP5);
        TextView txtTydP6 = (TextView) findViewById(R.id.txtTydP6);
        TextView txtTydPouse2 = (TextView) findViewById(R.id.txtTydPouse2);
        TextView txtTydP7 = (TextView) findViewById(R.id.txtTydP7);
        TextView txtTydP8 = (TextView) findViewById(R.id.txtTydP8);

        if ((dag.equals("Maandag") && read("com.sps.jvr.maandagTye").equals("false")) || !read("com.sps.jvr.maandagTye").equals("false")){
            txtTydP1.setText("08:40 - 09:15");
            txtTydP2.setText("09:15 - 09:50");
            txtTydP3.setText("09:50 - 10:25");
            txtTydPouse1.setText("10:25 - 10:45");
            txtTydP4.setText("10:45 - 11:20");
            txtTydP5.setText("11:20 - 11:55");
            txtTydP6.setText("11:55 - 12:30");
            txtTydPouse2.setText("12:30 - 12:55");
            txtTydP7.setText("12:55 - 13:30");
            txtTydP8.setText("13:30 - 14:05");
        }else{
            txtTydP1.setText("08:00 - 08:40");
            txtTydP2.setText("08:40 - 09:20");
            txtTydP3.setText("09:20 - 10:00");
            txtTydPouse1.setText("10:00 - 10:20");
            txtTydP4.setText("10:20 - 11:00");
            txtTydP5.setText("11:00 - 11:40");
            txtTydP6.setText("11:40 - 12:20");
            txtTydPouse2.setText("12:20 - 12:45");
            txtTydP7.setText("12:45 - 13:25");
            txtTydP8.setText("13:25 - 14:05");
        }
    }

    public void setRDV(boolean set){
        if (set){
            ((TextView) findViewById(R.id.txtRDV)).setVisibility(View.VISIBLE);
        }
    }

    public void onDagAfClick(View view){
        switch (dagView){
            case "Maandag":
                dagView = "Dinsdag";
                break;
            case "Dinsdag":
                dagView = "Woensdag";
                break;
            case "Woensdag":
                dagView = "Donderdag";
                break;
            case "Donderdag":
                dagView = "Vrydag";
                break;
            case "Vrydag":
                dagView = "Maandag";
                break;
        }

        setDagText(dagView);
        setTye(dagView);
        setVakke(dagView);
        setVolgorde(read("com.sps.jvr.volgorde"));
        setRDV(JanTools.readBool("com.sps.jvr.RDV"));
    }

    public void onDagOpClick(View view){
        switch (dagView){
            case "Maandag":
                dagView = "Vrydag";
                break;
            case "Dinsdag":
                dagView = "Maandag";
                break;
            case "Woensdag":
                dagView = "Dinsdag";
                break;
            case "Donderdag":
                dagView = "Woensdag";
                break;
            case "Vrydag":
                dagView = "Donderdag";
                break;
        }

        setDagText(dagView);
        setTye(dagView);
        setVakke(dagView);
        setVolgorde(read("com.sps.jvr.volgorde"));
        setRDV(JanTools.readBool("com.sps.jvr.RDV"));
    }

    public void onTerugClick(View view){
        gotoActivity(MainScreen.class);
    }
    
    public void onBackPressed () {
        onTerugClick(findViewById(R.id.button6));
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void setDagText(String text){
        TextView txtDag = (TextView) findViewById(R.id.txtDag);
        txtDag.setText(text);
    }

    public void setVakke(String dag){
        String[] vakke = new String[8];

        for (int i = 0; i < 8; i++){    //Init vakke
            vakke[i] = read("com.sps.jvr." + dag.toLowerCase() + "P" + (i + 1));    //com.sps.jvr.maandagP1
        }

        if (!read("com.sps.jvr.volgordeDag", "false") && !read("com.sps.jvr.volgordeDag", "0")) {     //sort indien nodig
            int[] p = {1,2,3,4,5,6,7,8};
            if (!read("com.sps.jvr.volgorde", "12345678")) {
                for (int i = 0; i < 8; i++) {
                    p[i] = Integer.parseInt(read("com.sps.jvr.volgorde").substring(i, i + 1));
                }
                String[] tempV = new String[8];
                System.arraycopy(vakke, 0, tempV, 0, 8);
                for (int i = 0; i < 8; i++) {
                    vakke[i] = tempV[p[i] - 1];
                }
            }
        }

        for (int i = 0; i < 8; i++){    //update views
            int id = getResources().getIdentifier("txtVakP" + (i+1), "id", getPackageName());
            ((TextView) findViewById(id)).setText(vakke[i]);
        }
        ((TextView) findViewById(R.id.txtVakNaSkool)).setText(read("com.sps.jvr.naSkool" + dag.substring(0, 2)));
    }

    public void setBold(String currentPeriode){//todo not if naweek

        DateTime dt = new DateTime( new Date());
        if (dt.dayOfWeek().get() <= 5) { //weekday

            int parsed = Integer.parseInt(currentPeriode);

            if ((parsed >= 1 && parsed <= 3) || (parsed >= 5 && parsed <= 7) || parsed == 9 || parsed == 10) {

                if (parsed >= 9) parsed--;
                if (parsed >= 5) parsed--;

                ((TextView) findViewById(
                        getResources().getIdentifier("txtVakP" + parsed, "id", getPackageName())
                )).setTypeface(null, Typeface.BOLD);
                ((TextView) findViewById(
                        getResources().getIdentifier("txtTydP" + parsed, "id", getPackageName())
                )).setTypeface(null, Typeface.BOLD);
                ((TextView) findViewById(
                        getResources().getIdentifier("txtP" + parsed, "id", getPackageName())
                )).setTypeface(null, Typeface.BOLD);

            } else {

                if (parsed == 4) parsed = 1;
                else if (parsed == 8) parsed = 2;
                else return;//should never happen

                ((TextView) findViewById(
                        getResources().getIdentifier("txtTydPouse" + parsed, "id", getPackageName())
                )).setTypeface(null, Typeface.BOLD);
                ((TextView) findViewById(
                        getResources().getIdentifier("txtPouse" + parsed, "id", getPackageName())
                )).setTypeface(null, Typeface.BOLD);
            }
        }

    }

    public void setGrys(int periode){
        ((TextView) findViewById(
                getResources().getIdentifier("txtVakP" + periode, "id", getPackageName()))
        ).setTextColor(Color.GRAY);
        ((TextView) findViewById(
                getResources().getIdentifier("txtTydP" + periode, "id", getPackageName())
        )).setTextColor(Color.GRAY);
        ((TextView) findViewById(
                getResources().getIdentifier("txtP" + periode, "id", getPackageName())
        )).setTextColor(Color.GRAY);
    }

    public void setGrys(){
        for (int i = 1; i <= 8; i++){
            String text = ((TextView) findViewById(
                    getResources().getIdentifier("txtVakP" + i, "id", getPackageName())
            )).getText().toString();
            if (text.equalsIgnoreCase("Af")){
                setGrys(i);
            }
        }
    }

    public void setVolgorde(String volgStr){
        Calendar c = Calendar.getInstance();
        if (read("com.sps.jvr.volgordeDag", c.get(Calendar.DAY_OF_WEEK) + "") && !read("com.sps.jvr.volgordeDag", "false") && !read("com.sps.jvr.volgordeDag", "0")) {
            int[] volgorde = new int[8];
            for (int i = 0; i < 8; i++) {
                volgorde[i] = Integer.parseInt(volgStr.substring(i, i + 1));
            }
            for (int i = 0; i < 8; i++) {
                int Id = getResources().getIdentifier("txtP" + (i + 1), "id", getPackageName());
                ((TextView) findViewById(Id)).setText(String.valueOf((i + 1) + "/" + volgorde[i]));
            }
        }else{
            write("com.sps.jvr.volgordeDag", "false");
        }
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String in, String cmp){
        return read(in).equals(cmp);
    }
}


