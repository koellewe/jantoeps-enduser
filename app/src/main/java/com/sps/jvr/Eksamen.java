package com.sps.jvr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Eksamen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eksamen);
    }

    public void onSettingsClick(View view){
        ((ImageView) view).setImageResource(R.drawable.cogclicked);
        gotoActivity(Settings.class);
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }
}
