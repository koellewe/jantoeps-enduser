package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sps.jvr.utils.JanTools;

public class LeerderConfig3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leerder_config3);
        JanTools.setContext(this);

        assignAktiwiteite();
        setSpinners();
        setLaatBus();
        setSpinnerListeners();
    }

    private String[] aktiwiteite;
    private int itemSel = 0;
    private boolean stopNextSelect = false;

    private void setSpinnerListeners() {

        for (final String kortdag : JanTools.kortdae){

            Spinner itSpinner = (Spinner) findViewById(getResources().getIdentifier("spinner"+kortdag, "id", getPackageName()));
            final Spinner itSpinDup = itSpinner; //wasting resources like a boss

            itSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            itemSel++;

                            if (parentView.getSelectedItemPosition() == parentView.getAdapter().getCount()-1 && itemSel>5 && !stopNextSelect){ //last item & gone through all spinners

                                final EditText edt = new EditText(LeerderConfig3.this);
                                edt.setPadding(0, 20, 0, 20);
                                new AlertDialog.Builder(LeerderConfig3.this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Verskaf jou item")
                                        .setMessage("Verskaf jou 'custom' item:")
                                        .setView(edt)
                                        .setPositiveButton("Klaar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                String newName = edt.getText().toString().trim();
                                                if (newName.isEmpty() || newName.equalsIgnoreCase("Iets Anders")) {
                                                    JanTools.makeToast("Item ongeldig!");
                                                    itSpinDup.setSelection(0);
                                                } else {
                                                    aktiwiteite[aktiwiteite.length-1] = newName;
                                                    resetAdapter(kortdag);
                                                    stopNextSelect = true;
                                                }
                                            }
                                        })
                                        .setNegativeButton("Kanselleer", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                itSpinDup.setSelection(0);
                                            }
                                        })
                                        .show();
                            }else if (stopNextSelect){
                                stopNextSelect = false;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            //do nothing
                        }
                    });
        }
    }

    private void resetAdapter(String dag) {

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, aktiwiteite);  //android.R.layout.simple_dropdown_item_1line
        Spinner spin = ((Spinner) findViewById(getResources().getIdentifier("spinner"+dag, "id", getPackageName())));
        spin.setAdapter(adapter);
        spin.setSelection(adapter.getCount()-1);

    }

    private void assignAktiwiteite() {
        aktiwiteite = new String[]{
                "[Niks]",
                "Rugby",
                "Hokkie",
                "Netbal",
                "Krieket",
                "Landloop",
                "Gholf",
                "Branderplank",
                "Atletiek",
                "Tutoring",
                "Rotary",
                "Debat",
                "Swem",
                "Tweekamp",
                "Musiek",
                "Cheerleading",
                "Toneel",
                "Tennis",
                "Iets Anders"
        };
    }

    public void setSpinners(){

        int maxpos = aktiwiteite.length-1;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, aktiwiteite);  //android.R.layout.simple_dropdown_item_1line

        boolean customItem = false;

        for (String kortdag : JanTools.kortdae){

            Spinner currSpin = (Spinner) findViewById(getResources().getIdentifier("spinner"+kortdag, "id", getPackageName()));

            currSpin.setAdapter(adapter);

            int pos = 0;
            while(!JanTools.read("naSkool"+kortdag, currSpin.getSelectedItem().toString()) && pos <= maxpos){
                pos++;
                if (pos <= maxpos){
                    currSpin.setSelection(pos);
                }
            }
            if (pos > maxpos){ //nie in lys
                if (JanTools.read("naSkool"+kortdag, "[Niks]")) {
                    currSpin.setSelection(0);
                } else { //het 'n waarde
                    currSpin.setSelection(maxpos);
                    customItem = true;
                }
            }

        }

        if (customItem && !JanTools.readBool("dnsCustomNaSkool")){ //dns = do not show
            final CheckBox chkAgain = new CheckBox(this);
            chkAgain.setText("Moet nie weer die wys nie.");
            new AlertDialog.Builder(this)
                    .setTitle("Custom Items")
                    .setMessage("Ek sien jy het 'custom' naskoolse items. Weens redes kan dit nie gewys word nie, maar dis nog steeds gestoor. Druk op 'Iets Anders' om dit te verander.")
                    .setIcon(android.R.drawable.ic_menu_edit)
                    .setView(chkAgain)
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (chkAgain.isChecked()){
                                JanTools.write("dnsCustomNaSkool", "true");
                            }
                        }
                    })
                    .show();
        }

    }

    public void setLaatBus(){
        ToggleButton tgl = (ToggleButton) findViewById(R.id.tglLaatBus);
        if (read("com.sps.jvr.laatBus", "true")){
            tgl.toggle();
        }
    }

    public void storeSpinners(){
        for (String kortdag : JanTools.kortdae){
            String selItem = ((Spinner) findViewById(getResources().getIdentifier("spinner"+kortdag, "id", getPackageName()))).getSelectedItem().toString();
            if (!selItem.equals("Iets Anders")){
                JanTools.write("naSkool"+kortdag, selItem);
            }
        }
    }

    public void onLaatBusClick(View view) {
        boolean checked = ((ToggleButton) view).isChecked();
        JanTools.write("laatBus", checked + "");
    }

    public void onTerugClick(View view){
        storeSpinners();
        Toast.makeText(this, "Stellings gestoor.", Toast.LENGTH_SHORT).show();
        Intent from = getIntent();
        if (from.hasExtra("next")){
            gotoActivity((Class) from.getExtras().get("next"));
        }else {
            gotoActivity(Settings.class);
        }
    }

    public void onBackPressed() {
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String read, String cmp){
        return read(read).equals(cmp);
    }

    public void onMissingClick(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Ontbreking")
                .setMessage("Is daar 'n vak nie hier nie? Kort daar 'n naskoolse aktiwiteit? Is die periodes verkeerd?\nAs daar enigiets fout is, laat weet my onmiddelik!")
                .setPositiveButton("Gee Terugvoer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LeerderConfig3.this, Feedback.class);
                        startActivity(intent);
                        //purposefully not finish()ing so that user can return to this activity as it was
                    }
                })
                .setNegativeButton("Nvm", null)
                .show();
    }
}
