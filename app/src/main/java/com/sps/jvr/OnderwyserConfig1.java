package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.sps.jvr.utils.JanTools;


public class OnderwyserConfig1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onderwyser_config1);
        JanTools.setContext(this);
        setNaam();
        setGrade();
        setVakke();
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onTerugClick(View view){
        gotoActivity(StartScreen.class);
    }
    
    public void onBackPressed () {
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void onVolgendeClick(View view) {
        String naam = ((EditText) findViewById(R.id.edtNaam)).getText().toString();

        String gradeString = "";

        for (int i = 8; i <= 12; i++) {
            int Id = getResources().getIdentifier("chk" + i, "id", getPackageName());
            if (((CheckBox) findViewById(Id)).isChecked()) gradeString += i + ",";
        }

        String vakke[] = {
                "Wisk", //0
                "Wisk G",
                "Afr",
                "Eng Home",
                "Eng Ad",
                "LO",
                "MBK",
                "NW",
                "EBW",
                "Teg",
                "FisWet", //10
                "BSGH",
                "Gasvry",
                "Gesk",
                "Bio",
                "Rek",
                "Toer",
                "IGO",
                "RTT",
                "Musiek",
                "VisKuns", //20
                "IT",
                "SkepKuns",
                "Ekon",
                "Geo" //24
        };

        String vakkeString = "";

        for (int i = 0; i < 25; i++) {
            int Id = getResources().getIdentifier("checkBox" + (i + 1), "id", getPackageName());
            if (((CheckBox) findViewById(Id)).isChecked()) vakkeString += vakke[i] + ",";
        }

        if (naam.equals("")) {
            JanTools.makeToast("Tik jou naam bo in.");
        } else if (vakkeString.equals("") || gradeString.equals("")) {
            JanTools.makeToast("Kies minstens een graad en een vak./l");
        } else {
            //passed all tests
            JanTools.write("com.sps.jvr.naam", naam);
            JanTools.write("com.sps.jvr.grade", gradeString);
            JanTools.write("com.sps.jvr.gegeweVakke", vakkeString);

            gotoActivity(OnderwyserConfig2.class);
        }
    }

    public void setNaam(){
        String naam = JanTools.read("com.sps.jvr.naam");
        if (!naam.equals("0")){
            EditText edt = (EditText) findViewById(R.id.edtNaam);
            edt.setText(naam);
        }
    }

    public void setGrade(){
        int commas = 0;
        String gradeString = JanTools.read("com.sps.jvr.grade");
        for (int i = 0; i < gradeString.length(); i++){
            if (gradeString.substring(i, i+1).equals(",")) commas++;
        }
        String grade[] = new String[commas];
        int prevI = 0;
        int j = 0;
        for (int i = 0; i < gradeString.length(); i++) {
            if (gradeString.substring(i, i + 1).equals(",")) {
                grade[j] = gradeString.substring(prevI, i);
                prevI = i + 1;
                j++;
            }
        }

        for (String temp : grade){
            for (int i = 8; i <= 12; i++){
                CheckBox chk = (CheckBox) findViewById(
                        getResources().getIdentifier("chk" + i, "id", getPackageName())
                );
                if (temp.equals(chk.getText().toString())){
                    chk.toggle();
                }
            }
        }
    }

    public void setVakke(){
        String alleVakke[] = {  //elkeen represent 'n checkbox
                "Wisk", //0
                "Wisk G",
                "Afr",
                "Eng Home",
                "Eng Ad",
                "LO",
                "MBK",
                "NW",
                "EBW",
                "Teg",
                "FisWet", //10
                "BSGH",
                "Gasvry",
                "Gesk",
                "Bio",
                "Rek",
                "Toer",
                "IGO",
                "RTT",
                "Musiek",
                "VisKuns", //20
                "IT",
                "SkepKuns",
                "Ekon",
                "Geo" //24
        };

        int commas = 0;
        String vakkeString = JanTools.read("com.sps.jvr.gegeweVakke");
        for (int i = 0; i < vakkeString.length(); i++){
            if (vakkeString.substring(i, i+1).equals(",")) commas++;
        }
        String vakke[] = new String[commas];
        int prevI = 0;
        int j = 0;
        for (int i = 0; i < vakkeString.length(); i++) {
            if (vakkeString.substring(i, i + 1).equals(",")) {
                vakke[j] = vakkeString.substring(prevI, i);
                prevI = i + 1;
                j++;
            }
        }

        for (String tmpVak : vakke){    //gaan deur gegewe vakke
            for (int i = 0; i < alleVakke.length; i++){     //gaan deur alleVakke
                if (tmpVak.equals(alleVakke[i])){       //vind gegewe vak in alleVakke
                    CheckBox chk = (CheckBox) findViewById(
                            getResources().getIdentifier("checkBox" + (i + 1), "id", getPackageName())  //vind matching checkbox
                    );
                    chk.toggle();   //check matching checkbox
                }
            }
        }
    }

    public void onMissingClick(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Ontbreking")
                .setMessage("Is daar 'n vak nie hier nie? Kort daar 'n naskoolse aktiwiteit? Is die periodes verkeerd?\nAs daar enigiets fout is, laat weet my onmiddelik!")
                .setPositiveButton("Gee Terugvoer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(OnderwyserConfig1.this, Feedback.class);
                        startActivity(intent);
                        //purposefully not finish()ing so that user can return to this activity as it was
                    }
                })
                .setNegativeButton("Nvm", null)
                .show();
    }

}
