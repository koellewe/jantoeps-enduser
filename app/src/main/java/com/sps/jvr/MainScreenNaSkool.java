package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sps.jvr.utils.JanTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class MainScreenNaSkool extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen_na_skool);
        JanTools.setContext(this);

        if (read("com.sps.jvr.vakansie", "true")){
            gotoActivity(Vakansie.class);
        }else {

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR) - (Integer.parseInt(read("com.sps.jvr.graad")) + 6);      //2015 - (10 + 6) = 1999
            if (JanTools.read("graad", "0")) year = calendar.get(Calendar.YEAR) - 38; //avg onnie ouderdom
            if (!JanTools.read("geboorteJaar", "0")){
                year = Integer.parseInt(JanTools.read("geboorteJaar"));
            }

            AdView mAdView = (AdView) findViewById(R.id.adView);

            AdRequest.Builder builder = new AdRequest.Builder()
                    .setBirthday(new GregorianCalendar(year, 1, 1).getTime());
            //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

            if (!JanTools.read("geslag", "0")){
                switch (JanTools.read("geslag")){
                    case "Vroulik":
                        builder.setGender(AdRequest.GENDER_FEMALE);
                        break;
                    case "Manlik":
                        builder.setGender(AdRequest.GENDER_MALE);
                        break;
                    case "Ander":
                        builder.setGender(AdRequest.GENDER_UNKNOWN);
                        break;
                }
            }

            mAdView.loadAd(builder.build());

            dagVanDieWeek = translateToAfr(calendar.get(Calendar.DAY_OF_WEEK));

            setVakke(dagVanDieWeek);

            if ((read("com.sps.jvr.naam").equals("0") || JanTools.read("geslag", "0")) && !JanTools.readBool("dnsLeeProfiel")) {
                ((TextView) findViewById(R.id.txtHallo)).setText(R.string.jounaam_bold);
                final CheckBox chk = new CheckBox(this);
                chk.setText("Moet nie hierdie weer wys nie.");

                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Leë Profiel")
                        .setMessage("Lyk my jy het nog nie jou profiel opgestel nie. Wanneer wil jy dit doen?")
                        .setView(chk)
                        .setPositiveButton("Nou", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (chk.isChecked()){
                                    JanTools.write("dnsLeeProfiel", "true");
                                }
                                gotoActivity(Profile.class);
                            }
                        })
                        .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (chk.isChecked()){
                                    JanTools.write("dnsLeeProfiel", "true");
                                }
                            }
                        })
                        .show();

            } else if (JanTools.read("naam", "0")) {
                ((TextView) findViewById(R.id.txtHallo)).setText(R.string.jounaam_bold);
            }else{
                ((TextView) findViewById(R.id.txtHallo)).setText("Hallo, " + read("com.sps.jvr.naam") + "!");
            }
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        if (JanTools.readBool("sync")) {
            new GetAfkondigings().execute();
        }
    }

    private ProgressDialog dialog;
    class GetAfkondigings extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainScreenNaSkool.this);
            dialog.setMessage("Verkry tans afkondigings...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... v) {

            try {

                HttpURLConnection httpCon = (HttpURLConnection) new URL(MainScreen.url_get_afks).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    //System.out.println("RESPONSE = " + sb.toString());

                    return new JSONObject(sb.toString());

                } else {
                    JanTools.makeToast("HTTP Flater");
                }

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(final JSONObject json) {
            //super.onPostExecute(s);
            dialog.dismiss();

            if (json == null) {
                JanTools.makeToast("Kon nie afkondigings kry nie.");
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        postCustom(json);
                    }
                });
            }
        }

        private void postCustom(JSONObject json){

            try {
                JSONArray jarr = json.getJSONArray("afkondigings");
                StringBuilder afkRaw = new StringBuilder();

                for (int i = 0; i < jarr.length(); i++){
                    afkRaw.append(((JSONObject) jarr.get(i)).getString("afkondiging"));
                    afkRaw.append(";");//seperator
                }

                JanTools.write("afkondigings", afkRaw.toString());

                setGebeurtenisse();

            } catch (JSONException e) {
                e.printStackTrace();
                JanTools.makeToast("Kon nie afkondigings verwerk nie.");
            }

        }
    }

    private void setGebeurtenisse() {
        ((TextView) findViewById(R.id.txtGebeurTitel)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.imgPyl)).setVisibility(View.VISIBLE);
        TextView txt = (TextView) findViewById(R.id.txtGebeur);

        //nuwe updates hier ook
        String display = "";
        if (JanTools.readBool("com.sps.jvr.weird")) {
            display += "• Tye is nie vandag normaal nie.\n";    //LW: as jy '???' sien, vervang dit met • (ALT+0149)
        }

        if (!JanTools.read("com.sps.jvr.afkondigings", "false")) {

            String afkondRaw = JanTools.read("afkondigings");

            int prevI = 0;
            for (int i = 0; i < afkondRaw.length(); i++) {   //count semis
                if (afkondRaw.substring(i, i + 1).equals(";")) {
                    display += "• " + afkondRaw.substring(prevI, i) + "\n";
                    prevI = i + 1;
                }
            }
        }

        if (MainScreen.checkNuweWeergawe(MainScreenNaSkool.this)) {
            display += "• Daar is 'n nuwe weergawe van die toeps beskikbaar.\n";
        }
        //more ifs with \n

        txt.setText(display);
    }


    //public Variables
    public String dagVanDieWeek = "Maandag";
    //en dof public variables

    public void setVakke(String dag){
        TextView txtVakP1 = (TextView) findViewById(R.id.txtVakP1);
        TextView txtVakP2 = (TextView) findViewById(R.id.txtVakP2);
        TextView txtVakP3 = (TextView) findViewById(R.id.txtVakP3);
        TextView txtVakP4 = (TextView) findViewById(R.id.txtVakP4);
        TextView txtVakP5 = (TextView) findViewById(R.id.txtVakP5);
        TextView txtVakP6 = (TextView) findViewById(R.id.txtVakP6);
        TextView txtVakP7 = (TextView) findViewById(R.id.txtVakP7);
        TextView txtVakP8 = (TextView) findViewById(R.id.txtVakP8);
        TextView txtMore = (TextView) findViewById(R.id.txtMore);
        TextView txtNaSkool = (TextView) findViewById(R.id.txtNaskool);

        switch (dag) {
            case "Sondag":
                txtVakP1.setText(read("com.sps.jvr.maandagP1"));
                txtVakP2.setText(read("com.sps.jvr.maandagP2"));
                txtVakP3.setText(read("com.sps.jvr.maandagP3"));
                txtVakP4.setText(read("com.sps.jvr.maandagP4"));
                txtVakP5.setText(read("com.sps.jvr.maandagP5"));
                txtVakP6.setText(read("com.sps.jvr.maandagP6"));
                txtVakP7.setText(read("com.sps.jvr.maandagP7"));
                txtVakP8.setText(read("com.sps.jvr.maandagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolMa"));
                txtMore.setText("Jy het môre die volgende vakke:");

                break;
            case "Maandag":
                txtVakP1.setText(read("com.sps.jvr.dinsdagP1"));
                txtVakP2.setText(read("com.sps.jvr.dinsdagP2"));
                txtVakP3.setText(read("com.sps.jvr.dinsdagP3"));
                txtVakP4.setText(read("com.sps.jvr.dinsdagP4"));
                txtVakP5.setText(read("com.sps.jvr.dinsdagP5"));
                txtVakP6.setText(read("com.sps.jvr.dinsdagP6"));
                txtVakP7.setText(read("com.sps.jvr.dinsdagP7"));
                txtVakP8.setText(read("com.sps.jvr.dinsdagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolDi"));
                txtMore.setText("Jy het môre die volgende vakke:");

                break;
            case "Dinsdag":
                txtVakP1.setText(read("com.sps.jvr.woensdagP1"));
                txtVakP2.setText(read("com.sps.jvr.woensdagP2"));
                txtVakP3.setText(read("com.sps.jvr.woensdagP3"));
                txtVakP4.setText(read("com.sps.jvr.woensdagP4"));
                txtVakP5.setText(read("com.sps.jvr.woensdagP5"));
                txtVakP6.setText(read("com.sps.jvr.woensdagP6"));
                txtVakP7.setText(read("com.sps.jvr.woensdagP7"));
                txtVakP8.setText(read("com.sps.jvr.woensdagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolWo"));
                txtMore.setText("Jy het môre die volgende vakke:");

                break;
            case "Woensdag":
                txtVakP1.setText(read("com.sps.jvr.donderdagP1"));
                txtVakP2.setText(read("com.sps.jvr.donderdagP2"));
                txtVakP3.setText(read("com.sps.jvr.donderdagP3"));
                txtVakP4.setText(read("com.sps.jvr.donderdagP4"));
                txtVakP5.setText(read("com.sps.jvr.donderdagP5"));
                txtVakP6.setText(read("com.sps.jvr.donderdagP6"));
                txtVakP7.setText(read("com.sps.jvr.donderdagP7"));
                txtVakP8.setText(read("com.sps.jvr.donderdagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolDo"));
                txtMore.setText("Jy het môre die volgende vakke:");

                break;
            case "Donderdag":
                txtVakP1.setText(read("com.sps.jvr.vrydagP1"));
                txtVakP2.setText(read("com.sps.jvr.vrydagP2"));
                txtVakP3.setText(read("com.sps.jvr.vrydagP3"));
                txtVakP4.setText(read("com.sps.jvr.vrydagP4"));
                txtVakP5.setText(read("com.sps.jvr.vrydagP5"));
                txtVakP6.setText(read("com.sps.jvr.vrydagP6"));
                txtVakP7.setText(read("com.sps.jvr.vrydagP7"));
                txtVakP8.setText(read("com.sps.jvr.vrydagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolVr"));
                txtMore.setText("Jy het môre die volgende vakke:");

                break;
            case "Saterdag":
            case "Vrydag":
                txtVakP1.setText(read("com.sps.jvr.maandagP1"));
                txtVakP2.setText(read("com.sps.jvr.maandagP2"));
                txtVakP3.setText(read("com.sps.jvr.maandagP3"));
                txtVakP4.setText(read("com.sps.jvr.maandagP4"));
                txtVakP5.setText(read("com.sps.jvr.maandagP5"));
                txtVakP6.setText(read("com.sps.jvr.maandagP6"));
                txtVakP7.setText(read("com.sps.jvr.maandagP7"));
                txtVakP8.setText(read("com.sps.jvr.maandagP8"));
                txtNaSkool.setText(read("com.sps.jvr.naSkoolMa"));
                txtMore.setText("Jy het Maandag die volgende vakke:");
                break;
        }
    }

    public void onHalloClick(View view){
        gotoActivity(Profile.class);
    }

    public String translateToAfr(int input){
        switch (input){
            case 1:
                return "Sondag";
            case 2:
                return "Maandag";
            case 3:
                return "Dinsdag";
            case 4:
                return "Woensdag";
            case 5:
                return "Donderdag";
            case 6:
                return "Vrydag";
            case 7:
                return "Saterdag";
        }
        return "Error";
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onSettingsClick(View view){
        ((ImageView) view).setImageResource(R.drawable.cogclicked);
        gotoActivity(Settings.class);
    }

    public void onVolleRoosterClick(View view){
        Calendar c = Calendar.getInstance();
        if (!read("com.sps.jvr.switchVakke").equals("false") && !read("com.sps.jvr.switchVakke").equals("0")){
            write("com.sps.jvr.tempSienDagRooster", read("com.sps.jvr.switchVakke"));
        }else{
            write("com.sps.jvr.tempSienDagRooster", translateToAfr(c.get(Calendar.DAY_OF_WEEK)));
        }
        gotoActivity(VolleRooster.class);
    }

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String key, String cmp){
        return read(key).equals(cmp);
    }
}
