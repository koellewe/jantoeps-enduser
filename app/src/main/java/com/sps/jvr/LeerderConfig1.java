package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sps.jvr.utils.JanTools;


public class LeerderConfig1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leerder_config1);
        write("com.sps.jvr.configDone", "false");
        JanTools.setContext(this);

        String[] groep1Vakke = {
                "[Kies jou vak]",
                "FisWet",
                "BSGH",
                "Gasvry",
                "Gesk"
        };
        String[] groep2Vakke = {
                "[Kies jou vak]",
                "Bio",
                "Rek",
                "Toer",
                "IGO",
                "RTT",
                "Musiek"
        };
        String[] groep3Vakke = {
                "[Kies jou vak]",
                "IT",
                "Ekon",
                "Geo",
                "Bio",
                "VisKuns",
                "Musiek",
                "SkepKuns"
        };

        Spinner spinner = (Spinner) findViewById(R.id.spinnerGroep1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, groep1Vakke);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setEnabled(false);

        Spinner spinnerG2 = (Spinner) findViewById(R.id.spinnerGroep2);
        adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, groep2Vakke);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerG2.setAdapter(adapter);
        spinnerG2.setEnabled(false);

        Spinner spinnerG3 = (Spinner) findViewById(R.id.spinnerGroep3);
        adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, groep3Vakke);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerG3.setAdapter(adapter);
        spinnerG3.setEnabled(false);

        int i = 0;
        while(!spinner.getSelectedItem().toString().equals(read("com.sps.jvr.groep1")) && i <= groep1Vakke.length){
            i++;
            if(i<groep1Vakke.length) {
                spinner.setSelection(i);
            }
        } if (i>groep1Vakke.length){
            System.out.print("Vak not found for spinnerG1");
            spinner.setSelection(0);
        }
        i = 0;
        while(!spinnerG2.getSelectedItem().toString().equals(read("com.sps.jvr.groep2")) && i <= groep2Vakke.length){
            i++;
            if(i<groep2Vakke.length) {
                spinnerG2.setSelection(i);
            }
        } if (i>groep2Vakke.length){
            System.out.print("Vak not found for spinnerG2");
            spinnerG2.setSelection(0);
        }
        i = 0;
        while(!spinnerG3.getSelectedItem().toString().equals(read("com.sps.jvr.groep3")) && i <= groep3Vakke.length){
            i++;
            if(i<groep3Vakke.length) {
                spinnerG3.setSelection(i);
            }
        } if (i>groep3Vakke.length){
            System.out.print("Vak not found for spinnerG3");
            spinnerG3.setSelection(0);
        }

        if (read("com.sps.jvr.musiek").equals("true")){
            //Switch toggle = (Switch) findViewById(R.id.musiekSwitch);
            //toggle.setChecked(true);

            ToggleButton tgl = (ToggleButton) findViewById(R.id.musiekSwitch);
            setToggle(tgl, true);
        }

        RadioButton eng1 = (RadioButton) findViewById(R.id.HomeLang);
        RadioButton eng2 = (RadioButton) findViewById(R.id.SecondLang);
        switch (read("com.sps.jvr.engels")){
            case "Eng Home":
                eng1.setChecked(true);
                eng1.performClick();
                break;
            case "Eng Ad":
                eng2.setChecked(true);
                eng2.performClick();
                break;
        }

        RadioButton wisk1 = (RadioButton) findViewById(R.id.Wisk);
        RadioButton wisk2 = (RadioButton) findViewById(R.id.WiskG);
        switch (read("com.sps.jvr.wiskunde")){
            case "Wiskunde":
                wisk1.setChecked(true);
                wisk1.performClick();
                break;
            case "Wiskunde G":
                wisk2.setChecked(true);
                wisk2.performClick();
                break;
        }

        RadioButton g8 = (RadioButton) findViewById(R.id.graad8);
        RadioButton g9 = (RadioButton) findViewById(R.id.graad9);
        RadioButton g10 = (RadioButton) findViewById(R.id.graad10);
        RadioButton g11 = (RadioButton) findViewById(R.id.graad11);
        RadioButton g12 = (RadioButton) findViewById(R.id.graad12);
        switch (read("com.sps.jvr.graad")){
            case "8":
                g8.setChecked(true);
                g8.performClick();
                break;
            case "9":
                g9.setChecked(true);
                g9.performClick();
                break;
            case "10":
                System.out.println(read("com.sps.jvr.graad"));
                g10.setChecked(true);
                g10.performClick();
                break;
            case "11":
                g11.setChecked(true);
                g11.performClick();
                break;
            case "12":
                g12.setChecked(true);
                g12.performClick();
                break;
        }
        if (!read("com.sps.jvr.naam", "0")) {
            ((TextView) findViewById(R.id.edtNaam)).setText(read("com.sps.jvr.naam"));
        }

        ((TextView) findViewById(R.id.txtNaam)).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(LeerderConfig1.this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Skip setup")
                        .setMessage("Die toeps gaan nou jou profiel opstel met lukrake vakke. Is jy seker jy wil voort gaan?")
                        .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                skipSetup();
                            }
                        })
                        .setNegativeButton("Nee", null)
                        .show();
                return true;
            }
        });
    }

    //Public variables:
    public boolean musiekClicked = false;
    public boolean engClicked = false;
    public boolean wiskClicked = false;
    public boolean graadClicked = false;
    //End public variables

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void setToggle(View view, boolean enabled){
        ToggleButton tb = (ToggleButton) view;
        if (enabled){
            if (!tb.isChecked()){
                tb.toggle();
            }
        }else{
            if (tb.isChecked()){
                tb.toggle();
            }
        }
    }

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public void onTerugClick(View view){
        Spinner spinnerGroep1 = (Spinner) findViewById(R.id.spinnerGroep1);
        Spinner spinnerGroep2 = (Spinner) findViewById(R.id.spinnerGroep2);
        Spinner spinnerGroep3 = (Spinner) findViewById(R.id.spinnerGroep3);

        if (!spinnerGroep1.getSelectedItem().toString().equals("[Kies jou vak]") && !spinnerGroep3.getSelectedItem().toString().equals("[Kies jou vak]") && !spinnerGroep2.getSelectedItem().toString().equals("[Kies jou vak]") && Integer.parseInt(read("com.sps.jvr.graad")) == 10) {
            write("com.sps.jvr.groep1", spinnerGroep1.getSelectedItem().toString());
            write("com.sps.jvr.groep2", spinnerGroep2.getSelectedItem().toString());
            write("com.sps.jvr.groep3", spinnerGroep3.getSelectedItem().toString());
        }
        if (!((EditText) findViewById(R.id.edtNaam)).getText().toString().equals("")){
            write("com.sps.jvr.naam", ((EditText) findViewById(R.id.edtNaam)).getText().toString());
        }
        write("com.sps.jvr.switchVakke", "false");
        write("com.sps.jvr.switchVakkeDag", "false");
        write("com.sps.jvr.maandagTye", "false");

        gotoActivity(StartScreen.class);
    }
    
    public void  onBackPressed () {
        onTerugClick(findViewById(R.id.button2));
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String src, String cmp){
        return read(src).equals(cmp);
    }

    public void onEnglishClick(View view){
        engClicked = true;
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.HomeLang:
                if(checked){
                    write("com.sps.jvr.engels", "Eng Home");
                }
                break;
            case R.id.SecondLang:
                if(checked){
                    write("com.sps.jvr.engels", "Eng Ad");
                }
                break;
        }
    }

    public void onWiskClick(View view){
        wiskClicked = true;
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.Wisk:
                if(checked){
                    write("com.sps.jvr.wiskunde", "Wiskunde");
                }
                break;
            case R.id.WiskG:
                if(checked){
                    write("com.sps.jvr.wiskunde", "Wiskunde G");
                }
                break;
        }
    }

    public void onVolgendeClick(View view){
        RadioButton w = (RadioButton) findViewById(R.id.Wisk);
        RadioButton g = (RadioButton) findViewById(R.id.WiskG);
        if(w.isChecked()){write("com.sps.jvr.wiskunde", "Wiskunde");}
        else if (g.isChecked()){write("com.sps.jvr.wiskunde", "Wiskunde G");}
        RadioButton h = (RadioButton) findViewById(R.id.HomeLang);
        RadioButton s = (RadioButton) findViewById(R.id.SecondLang);
        engClicked = true;
        if (h.isChecked()) {
            write("com.sps.jvr.engels", "Eng Home");}
        else if(s.isChecked()){write("com.sps.jvr.engels", "Eng Ad");}

        if(!graadClicked){
            makeToast("Kies jou graad", "short");
        }else{

            if(!musiekClicked && Integer.parseInt(read("com.sps.jvr.graad"))<=9)  {
                //Switch toggle = (Switch) findViewById(R.id.musiekSwitch);   //Ingeval die user nie een keer ge click het op die toggle nie.
                ToggleButton tgl = (ToggleButton) findViewById(R.id.musiekSwitch);
                if (tgl.isChecked()) {
                    write("com.sps.jvr.musiek", "true");
                } else {
                    write("com.sps.jvr.musiek", "false");
                }
            }

            if((!wiskClicked)&&(Integer.parseInt(read("com.sps.jvr.graad"))>=10)) { //M.A.W. as user 'n senior is
                makeToast("Kies jou Wiskunde", "short");
            }else {
                if (!engClicked) {
                    makeToast("Kies jou Engels", "short");
                } else {
                    Spinner spinnerGroep1 = (Spinner) findViewById(R.id.spinnerGroep1);
                    Spinner spinnerGroep2 = (Spinner) findViewById(R.id.spinnerGroep2);
                    Spinner spinnerGroep3 = (Spinner) findViewById(R.id.spinnerGroep3);

                    if (spinnerGroep1.getSelectedItem().toString().equals("[Kies jou vak]") && Integer.parseInt(read("com.sps.jvr.graad")) >= 10) {
                        makeToast("Kies jou Groep 1", "short");
                    } else {
                        if (spinnerGroep2.getSelectedItem().toString().equals("[Kies jou vak]") && Integer.parseInt(read("com.sps.jvr.graad")) >= 10) {
                            makeToast("Kies jou Groep 2", "short");
                        } else {
                            if (spinnerGroep3.getSelectedItem().toString().equals("[Kies jou vak]") && Integer.parseInt(read("com.sps.jvr.graad")) >= 10) {
                                makeToast("Kies jou Groep 3", "short");
                            } else {
                                if (((EditText) findViewById(R.id.edtNaam)).getText().toString().equals("")) {
                                    makeToast("Gee jou naam", "short");
                                } else {
                                    //passed all tests
                                    write("com.sps.jvr.naam", ((EditText) findViewById(R.id.edtNaam)).getText().toString());

                                    if (Integer.parseInt(read("com.sps.jvr.graad")) >= 10) {
                                        write("com.sps.jvr.groep1", spinnerGroep1.getSelectedItem().toString());
                                        write("com.sps.jvr.groep2", spinnerGroep2.getSelectedItem().toString());
                                        write("com.sps.jvr.groep3", spinnerGroep3.getSelectedItem().toString());

                                        if (((RadioButton) findViewById(R.id.Wisk)).isChecked()){   //Failsafe to some shady bug. I wish I could say it is unnecessary
                                            JanTools.write("wiskunde", "Wiskunde");
                                        }else{
                                            JanTools.write("wiskunde", "Wiskunde G");
                                        }
                                    }else{
                                        JanTools.write("wiskunde", "Wiskunde");
                                    }


                                    makeToast("Stellings gestoor.", "short");
                                    gotoActivity(LeerderConfig2.class);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void onMusiekClick(View view){
        musiekClicked = true;
        //Switch toggle = (Switch) findViewById(R.id.musiekSwitch);
        ToggleButton tgl = (ToggleButton) findViewById(R.id.musiekSwitch);
        if (tgl.isChecked()){
            write("com.sps.jvr.musiek", "true");
        }else{
            write("com.sps.jvr.musiek", "false");
        }
    }

    public void setJuniorWidgets(boolean state){
        if(state) {
            RadioButton radio = (RadioButton) findViewById(R.id.HomeLang);
            radio.setEnabled(true);
            radio = (RadioButton) findViewById(R.id.SecondLang);
            radio.setEnabled(true);
        }

        //Switch switchToggle = (Switch) findViewById(R.id.musiekSwitch);
        ToggleButton tgl = (ToggleButton) findViewById(R.id.musiekSwitch);
        tgl.setEnabled(state);

        if(!state){
            TextView textView = (TextView) findViewById(R.id.musiekText);
            textView.setTextColor(Color.GRAY);
            if(tgl.isChecked()){
                tgl.toggle();
            }
        }else{
            TextView textView = (TextView) findViewById(R.id.musiekText);
            textView.setTextColor(Color.BLACK);
        }
    }

    public void setSeniorWidgets(boolean state){
        RadioButton radio = (RadioButton) findViewById(R.id.HomeLang);
        if(state) {
            radio.setEnabled(true);
            radio = (RadioButton) findViewById(R.id.SecondLang);
            radio.setEnabled(true);
        }
        radio = (RadioButton) findViewById(R.id.Wisk);
        radio.setEnabled(state);
        radio = (RadioButton) findViewById(R.id.WiskG);
        radio.setEnabled(state);
        Spinner spinner = (Spinner) findViewById(R.id.spinnerGroep1);
        spinner.setEnabled(state);
        spinner = (Spinner) findViewById(R.id.spinnerGroep2);
        spinner.setEnabled(state);
        spinner = (Spinner) findViewById(R.id.spinnerGroep3);
        spinner.setEnabled(state);

        if(!state){
            spinner = (Spinner) findViewById(R.id.spinnerGroep1);
            spinner.setSelection(0);
            TextView textView = (TextView) findViewById(R.id.groep1Text);
            textView.setTextColor(Color.GRAY);

            spinner = (Spinner) findViewById(R.id.spinnerGroep2);
            spinner.setSelection(0);
            textView = (TextView) findViewById(R.id.groep2Text);
            textView.setTextColor(Color.GRAY);

            spinner = (Spinner) findViewById(R.id.spinnerGroep3);
            spinner.setSelection(0);
            textView = (TextView) findViewById(R.id.groep3Text);
            textView.setTextColor(Color.GRAY);

            RadioButton radioButton = (RadioButton) findViewById(R.id.Wisk);
            if (!radioButton.isChecked()){
                radioButton.toggle();
            }
            radioButton = (RadioButton) findViewById(R.id.WiskG);
            if (radioButton.isChecked()){
                radioButton.toggle();
            }
        }else{
            TextView textView = (TextView) findViewById(R.id.groep1Text);
            textView.setTextColor(Color.BLACK);
            textView = (TextView) findViewById(R.id.groep2Text);
            textView.setTextColor(Color.BLACK);
            textView = (TextView) findViewById(R.id.groep3Text);
            textView.setTextColor(Color.BLACK);
        }
    }

    public void onGraadClick(View view){
        graadClicked = true;
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.graad8:
                if(checked){
                    setSeniorWidgets(false);
                    setJuniorWidgets(true);
                    write("com.sps.jvr.graad", "8");
                }
                break;
            case R.id.graad9:
                if(checked){
                    setSeniorWidgets(false);
                    setJuniorWidgets(true);
                    write("com.sps.jvr.graad", "9");
                }
                break;
            case R.id.graad10:
                if(checked){
                    setJuniorWidgets(false);
                    setSeniorWidgets(true);
                    write("com.sps.jvr.graad", "10");
                }
                break;
            case R.id.graad11:
                if(checked){
                    setJuniorWidgets(false);
                    setSeniorWidgets(true);
                    write("com.sps.jvr.graad", "11");
                }
                break;
            case R.id.graad12:
                if(checked){
                    setJuniorWidgets(false);
                    setSeniorWidgets(true);
                    write("com.sps.jvr.graad", "12");
                }
                break;
        }
    }

    public void onMissingClick(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Ontbreking")
                .setMessage("Is daar 'n vak nie hier nie? Kort daar 'n naskoolse aktiwiteit? Is die periodes verkeerd?\nAs daar enigiets fout is, laat weet my onmiddelik!")
                .setPositiveButton("Gee Terugvoer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LeerderConfig1.this, Feedback.class);
                        startActivity(intent);
                        //purposefully not finish()ing so that user can return to this activity as it was
                    }
                })
                .setNegativeButton("Nvm", null)
                .show();
    }

    public String dayOfWeek(int i){
        switch (i){
            case 1:
                return "maandag";
            case 2:
                return "dinsdag";
            case 3:
                return "woensdag";
            case 4:
                return "donderdag";
            case 5:
                return "vrydag";
        }
        return null;
    }

    public void skipSetup(){

        JanTools.write("groep1", "BSGH");
        JanTools.write("groep2", "RTT");
        JanTools.write("groep3", "Musiek");
        JanTools.write("engels", "Eng Ad");
        JanTools.write("wikskunde", "Wiskunde");

        String[] vakke = {
                "BSGH",
                "RTT",
                "Musiek",
                "Afrikaans",
                "Eng Ad",
                "Wiskunde",
                "LO",
                "Wiskunde",
                "MBK"
        };

        for (int i = 1; i <= 5; i++){
            for (int j = 1; j <= 8; j++){
                JanTools.write(dayOfWeek(i) + "P" + j, JanTools.randArr(vakke));
            }
        }

        JanTools.write("configDone", "true");

        startActivity(new Intent(this, FirstRun.class));
        finish();
    }

}

