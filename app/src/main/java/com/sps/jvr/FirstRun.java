package com.sps.jvr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.Window;
import android.widget.TextView;

import com.sps.jvr.utils.JanTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirstRun extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(null);
        setContentView(R.layout.activity_first_run);
        JanTools.setContext(this);


        if (JanTools.readBool("com.sps.jvr.configDone")) {
            dlUpdate();
        }else{
            gotoActivity(StartScreen.class);
        }

        setNaSkool();   //basies setdefault vir al die naskoolse akt
        setDefaultValue("com.sps.jvr.ads", "true");
        setDefaultValue("com.sps.jvr.sync", "true");
        setDefaultValue("com.sps.jvr.vakansie", "false");
        setDefaultValue("com.sps.jvr.configDone", "false");
        setDefaultValue("com.sps.jvr.version", "false");
        setDefaultValue("com.sps.jvr.vibreer", "false");
        setDefaultValue("gebeurtenis", "false");
        setDefaultValue("wifi", "false");
        setDefaultValue("switchVakke", "false");
        setDefaultValue("switchVakkeDag", "false");
        setDefaultValue("maandagTye", "false");
        setDefaultValue("volgordeDag", "false");
        setDefaultValue("catchphrase", "false");
        setDefaultValue("dnsCustomNaSkool", "false");
        setDefaultValue("dnsLeeProfiel", "false");
        setDefaultValue("devEnabled", "false");
    }

    public void gtfo(){
        if (JanTools.readBool("com.sps.jvr.configDone")){
            gotoActivity(MainScreen.class);
        }else{
            gotoActivity(StartScreen.class);
        }
    }

    //public String UPDATE_PATH_FULL = this.getFilesDir().getPath() + "/toepsData.txt";
    //public String UPDATE_PATH = this.getFilesDir().getPath();
    public String UPDATE_URL =
            //"https://dl.dropboxusercontent.com/s/hxijtyz1sscpgyn/update.txt"  //first: one line
            //"https://dl.dropboxusercontent.com/s/ovucv7gof3s8w5h/toepsData.txt"   //three lines
            //"https://onedrive.live.com/download?resid=DBD0655A526FB342!23429&authkey=!AOGabdtEGynQQsA&ithint=file%2ctxt"    //onedrive for online editing
            //"https://onedrive.live.com/download?resid=DBD0655A526FB342!20508&authkey=!AP9t-vgWa-MaS4c&ithint=file%2ctxt"  //root folder for easy updating from admin app
            //"http://rauten.ddns.net/getUpdate.php"    //self-hosted vir even easier updating via ftp asook super vinnige klein html file
            //"http://rauten.ddns.net/jantoeps/getUpdate.php"   //moved for sorting's sake
            //"http://rauten.co.za/apps/jantoeps/update/"  //domain purchase
            "http://apps.rauten.co.za/jantoeps/get_all_updates.php" //moved for sorting + updated for DB access
            ;

    public void dlUpdate(){
        if (JanTools.readBool("sync") && (!JanTools.readBool("wifi") || (JanTools.readBool("wifi") && JanTools.checkIfWifi()))) {   //sync && (!wifi || (wifi && wifiCon))

            if (JanTools.checkConnection()) {

                new GetUpdates().execute(UPDATE_URL);

            }else{
                setSyncFalse();
                JanTools.makeToast("Sink flater: Geen konneksie.");
                gtfo();
            }
        }else{
            setSyncFalse();
            gtfo();
        }
    }



    class GetUpdates extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HttpURLConnection httpCon = (HttpURLConnection) new URL(args[0]).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    System.out.println("RESPONSE = " + sb.toString());

                    return new JSONObject(sb.toString());

                } else {
                    JanTools.makeToast("HTTP Error");
                }

            } catch (JSONException |IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        public void onPostExecute(JSONObject json){

            if (json != null){

                try {
                    JSONArray jarr = json.getJSONArray("updates");
                    String[] keys = new String[jarr.length()];
                    String[] vals = new String[jarr.length()];

                    for (int i = 0; i < jarr.length(); i++){
                        keys[i] = ((JSONObject) jarr.get(i)).getString("name");
                        vals[i] = ((JSONObject) jarr.get(i)).getString("state");
                    }

                    //ala fake db, courtesy Jans (c) 2015

                    JanTools.write("weird", vals[JanTools.indexOfStrArr(keys, "weird_tye")]);
                    JanTools.write("eksamen", vals[JanTools.indexOfStrArr(keys, "eksamen")]);
                    JanTools.write("vakansie", vals[JanTools.indexOfStrArr(keys, "vakansie")]);
                    JanTools.write("version", vals[JanTools.indexOfStrArr(keys, "latest_version")]);

                    //Nuwe item: hier, setsyncfalse, setdefault (oncreate), mainscreen.setgebeurtenisse

                } catch (JSONException e) {
                    e.printStackTrace();
                    JanTools.makeToast("Sink flater");
                    setSyncFalse();
                    gtfo();
                }

                gtfo();

            }else {
                JanTools.makeToast("Sink flater");
                setSyncFalse();
                gtfo();
            }

        }

    }

    public void setSyncFalse(){
        JanTools.write("com.sps.jvr.weird", "false");
        JanTools.write("com.sps.jvr.eksamen", "false");
        JanTools.write("com.sps.jvr.vakansie", "false");
        JanTools.write("com.sps.jvr.version", "false");
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public String read(String key) {
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public boolean read(String in, String cmp){
        return read(in).equals(cmp);
    }

    public void setDefaultValue(String key, String def){
        if (JanTools.read(key, "0")){
            JanTools.write(key, def);
        }
    }

    public void setNaSkool(){
        if (read("com.sps.jvr.naSkoolMa").equals("0")){
            write("com.sps.jvr.naSkoolMa", "[Niks]");
        }if (read("com.sps.jvr.naSkoolDi").equals("0")){
            write("com.sps.jvr.naSkoolDi", "[Niks]");
        }if (read("com.sps.jvr.naSkoolWo").equals("0")){
            write("com.sps.jvr.naSkoolWo", "[Niks]");
        }if (read("com.sps.jvr.naSkoolDo").equals("0")){
            write("com.sps.jvr.naSkoolDo", "[Niks]");
        }if (read("com.sps.jvr.naSkoolVr").equals("0")){
            write("com.sps.jvr.naSkoolVr", "[Niks]");
        }
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    public String translateToAfr(int input){
        switch (input){
            case 1:
                return "Sondag";
            case 2:
                return "Maandag";
            case 3:
                return "Dinsdag";
            case 4:
                return "Woensdag";
            case 5:
                return "Donderdag";
            case 6:
                return "Vrydag";
            case 7:
                return "Saterdag";
        }
        return null;
    }
}



