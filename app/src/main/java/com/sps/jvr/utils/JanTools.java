package com.sps.jvr.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Random;

/**
 * Created by Jans Rautenbach on 2015/09/14.
 */

public class JanTools extends Activity {

    static public void setContext(Context context){
        ctx = context;
    }

    static public Context ctx;

    static public String[] kortdae = {
            "Ma",
            "Di",
            "Wo",
            "Do",
            "Vr"
    };

    static public String[] langdae = {
            "Maandag",
            "Dinsdag",
            "Woensdag",
            "Donderdag",
            "Vrydag"
    };

    static public void makeToast(final String s){
        ((Activity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (s.length() < 2){
                    Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                }else if (s.substring(s.length() - 2).equals("/l")){
                    Toast.makeText(ctx, s.substring(0, s.length() - 2), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    static public int indexOfStrArr(String[] array, String search){

        int pos = 0;
        while (!array[pos].equals(search)){
            pos++;
            if (pos >= array.length){
                return -1;
            }
        }
        return pos;

    }

    public static String dayToDagLank(int input) {
        switch (input) {
            case 1:
                return "Sondag";
            case 2:
                return "Maandag";
            case 3:
                return "Dinsdag";
            case 4:
                return "Woensdag";
            case 5:
                return "Donderdag";
            case 6:
                return "Vrydag";
            case 7:
                return "Saterdag";
        }
        return "Error";
    }

    public static String dayToDagKort(int input) {
        switch (input) {
            case 1:
                return "So";
            case 2:
                return "Ma";
            case 3:
                return "Di";
            case 4:
                return "Wo";
            case 5:
                return "Do";
            case 6:
                return "Vr";
            case 7:
                return "Sa";
        }
        return "Error";
    }

    static public void write(String key, String val){
        SharedPreferences sharedPref = ctx.getSharedPreferences("com.sps.jvr.info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        if (key.length() > "com.sps.jvr.".length()) {
            if (!key.substring(0, "com.sps.jvr.".length()).equals("com.sps.jvr.")) {
                key = "com.sps.jvr." + key;
            }
        }else{
            key = "com.sps.jvr." + key;
        }

        editor.putString(key, val);
        editor.apply();
    }

    static public String read(String key) {
        SharedPreferences sharedPref = ctx.getSharedPreferences("com.sps.jvr.info", MODE_PRIVATE);
        if (key.length() > "com.sps.jvr.".length()) {
            if (!key.substring(0, "com.sps.jvr.".length()).equals("com.sps.jvr.")) {
                key = "com.sps.jvr." + key;
            }
        }else{
            key = "com.sps.jvr." + key;
        }
        return sharedPref.getString(key, "0");
    }

    static public boolean read(String in, String cmp){
        return read(in).equals(cmp);
    }

    static public boolean readBool(String in){
        return read(in).equals("true");
    }

    static public int countLines(String filename){
        if (!(new File(filename)).exists()){
            return -1;

        }
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(filename));
            try {
                byte[] c = new byte[1024];
                int count = 0;
                int readChars = 0;
                boolean empty = true;
                while ((readChars = is.read(c)) != -1) {
                    empty = false;
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n') {
                            ++count;
                        }
                    }
                }
                return (count == 0 && !empty) ? 1 : count + 1;
            } finally {
                is.close();
            }
        }catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return -1;
    }

    public static void printArray(Object[] print){
        for (int i = 0; i < print.length; i++){
            System.out.println(("pos " + i + ": " + print[i]));
        }
    }

    static public boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    static public boolean checkIfWifi(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static float convertPixelsToDp(float px){
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    public static void writeLine(String line, BufferedWriter writer) throws Exception{
        writer.write(line);
        writer.newLine();
        writer.flush();
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        return rand.nextInt((max - min) + 1) + min;
    }

    public static String randArr(String[] array){
        return array[randInt(0, array.length-1)];
    }
}


