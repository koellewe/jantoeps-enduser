package com.sps.jvr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.jvr.utils.JanTools;

public class LeerderConfig2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leerder_config2);
        JanTools.setContext(this);

        int hoeveelheidVakke;
        if(Integer.parseInt(read("com.sps.jvr.graad"))>=10){
            hoeveelheidVakke = 9;
        }else if (read("com.sps.jvr.musiek").equals("true")){
            hoeveelheidVakke = 14;
        }else{
            hoeveelheidVakke = 13;
        }

        if(JanTools.read("wiskunde", "0")){ //double failsafe to shady bug in lconfig1
            JanTools.write("wiskunde", "Wiskunde");
        }

        String[] vakke = new String[hoeveelheidVakke];
        if(Integer.parseInt(read("com.sps.jvr.graad"))>=10){
            vakke[0] = "[Kies jou vak]";
            vakke[1] = read("com.sps.jvr.wiskunde");
            vakke[2] = read("com.sps.jvr.engels");
            vakke[3] = "Afrikaans";
            vakke[4] = "LO";
            vakke[5] = "MBK";
            vakke[6] = read("com.sps.jvr.groep1");
            vakke[7] = read("com.sps.jvr.groep2");
            vakke[8] = read("com.sps.jvr.groep3");
        }else{
            if (read("com.sps.jvr.musiek").equals("true")) {
                vakke[0] = "[Kies jou vak]";
                vakke[1] = "Wiskunde";
                vakke[2] = read("com.sps.jvr.engels");
                vakke[3] = "Afrikaans";
                vakke[4] = "LO";
                vakke[5] = "MBK";
                vakke[6] = "NW";
                vakke[7] = "Gesk";
                vakke[8] = "Geo";
                vakke[9] = "Musiek";
                vakke[10] = "SkepKuns";
                vakke[11] = "EBW";
                vakke[12] = "Rek";
                vakke[13] = "Teg";
            }else{
                vakke[0] = "[Kies jou vak]";
                vakke[1] = "Wiskunde";
                vakke[2] = read("com.sps.jvr.engels");
                vakke[3] = "Afrikaans";
                vakke[4] = "LO";
                vakke[5] = "MBK";
                vakke[6] = "NW";
                vakke[7] = "Gesk";
                vakke[8] = "Geo";
                vakke[9] = "SkepKuns";
                vakke[10] = "EBW";
                vakke[11] = "Rek";
                vakke[12] = "Teg";
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner_item, vakke);
        for (int i = 1; i <= 8; i++){
            ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
            )).setAdapter(adapter);
        }

        setSpinners(dayEditing);
    }

    //public variables
    public int dayEditing = 1;
    //end of public variables

    public void makeToast(String say, String length){
        Toast toast;
        if (length.equals("long")) {
            toast = Toast.makeText(this, say, Toast.LENGTH_LONG);
        }else{
            toast = Toast.makeText(this, say, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public void onVolgendeClick(View view){
        int unselectedSpinner = -1;
        for (int i = 1; i <= 8; i++){
            if (
                    ((Spinner) findViewById(
                            getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
                    )).getSelectedItem().toString().equals("[Kies jou vak]")
                    ) unselectedSpinner = i;
        }

        if (unselectedSpinner == -1){
            //good to go
            storeSpinners(dayEditing);

            dayEditing ++;
            if(dayEditing == 6){
                write("com.sps.jvr.configDone", "true");
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Naskoolse Aktiwiteite")
                        .setMessage("Jy is nou klaar met die belangrikste opstel-werk. Wil jy naskoolse aktiwiteite opstel?")
                        .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(LeerderConfig2.this, LeerderConfig3.class);
                                intent.putExtra("next", MainScreen.class);//skip settings
                                LeerderConfig2.this.startActivity(intent);
                                LeerderConfig2.this.finish();
                            }
                        })
                        .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                gotoActivity(MainScreen.class);
                            }
                        })
                        .setCancelable(false)
                        .show();
            }else{
                setDayTxt(dayEditing);
                setSpinners(dayEditing);
            }
        }else{
            JanTools.makeToast("Kies periode " + unselectedSpinner + " se vak.");
        }
    }

    public String intToDagVol(int i){
        switch (i){
            case 1:
                return "maandag";
            case 2:
                return "dinsdag";
            case 3:
                return "woensdag";
            case 4:
                return "donderdag";
            case 5:
                return "vrydag";
        }
        return null;
    }

    public void storeSpinners(int dag){
        String dagStr = intToDagVol(dag);
        for (int i = 1; i <= 8; i++){
            JanTools.write(dagStr + "P" + i,
                    ((Spinner) findViewById(
                            getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
                    )).getSelectedItem().toString()
                    );
        }

        makeToast("Periodes gestoor", "short");
    }

    public void setSpinners(int dag){
        /*Spinner spinnerP1 = (Spinner) findViewById(R.id.spinnerP1);
        Spinner spinnerP2 = (Spinner) findViewById(R.id.spinnerP2);
        Spinner spinnerP3 = (Spinner) findViewById(R.id.spinnerP3);
        Spinner spinnerP4 = (Spinner) findViewById(R.id.spinnerP4);
        Spinner spinnerP5 = (Spinner) findViewById(R.id.spinnerP5);
        Spinner spinnerP6 = (Spinner) findViewById(R.id.spinnerP6);
        Spinner spinnerP7 = (Spinner) findViewById(R.id.spinnerP7);
        Spinner spinnerP8 = (Spinner) findViewById(R.id.spinnerP8);
        int pos = 0;*/
        int maxPos;
        if(Integer.parseInt(read("com.sps.jvr.graad"))>=10){
            maxPos = 8;
        }else{
            if (JanTools.readBool("com.sps.jvr.musiek")){
                maxPos = 13;
            }else {
                maxPos = 12;
            }
        }

        String[] vakke = new String[maxPos+1];
        for (int i = 0; i < vakke.length; i++){
            vakke[i] = ((Spinner) findViewById(R.id.spinnerP1)).getAdapter().getItem(i).toString();
        }

        for (int i = 1; i <= 8; i++) {
            int index = JanTools.indexOfStrArr(vakke, JanTools.read(intToDagVol(dag) + "P" + i));
            if (index == -1) index = 0;
            ((Spinner) findViewById(
                    getResources().getIdentifier("spinnerP" + i, "id", getPackageName())
            )).setSelection(index);
        }

        /*switch (dag){
            case 1:
                while(!spinnerP1.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP1")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP1.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP1.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP2.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP2")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP2.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP2.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP3.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP3")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP3.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP3.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP4.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP4")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP4.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP4.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP5.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP5")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP5.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP5.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP6.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP6")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP6.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP6.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP7.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP7")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP7.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP7.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP8.getSelectedItem().toString().equals(read("com.sps.jvr.maandagP8")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP8.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP8.setSelection(0);
                    System.out.println("Periode not found");
                }
                break;

            case 2:
                pos = 0;
                while(!spinnerP1.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP1")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP1.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP1.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP2.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP2")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP2.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP2.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP3.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP3")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP3.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP3.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP4.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP4")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP4.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP4.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP5.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP5")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP5.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP5.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP6.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP6")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP6.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP6.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP7.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP7")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP7.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP7.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP8.getSelectedItem().toString().equals(read("com.sps.jvr.dinsdagP8")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP8.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP8.setSelection(0);
                    System.out.println("Periode not found");
                }
                break;

            case 3:
                pos = 0;
                while(!spinnerP1.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP1")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP1.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP1.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP2.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP2")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP2.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP2.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP3.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP3")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP3.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP3.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP4.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP4")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP4.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP4.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP5.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP5")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP5.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP5.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP6.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP6")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP6.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP6.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP7.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP7")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP7.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP7.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP8.getSelectedItem().toString().equals(read("com.sps.jvr.woensdagP8")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP8.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP8.setSelection(0);
                    System.out.println("Periode not found");
                }
                break;

            case 4:
                pos = 0;
                while(!spinnerP1.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP1")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP1.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP1.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP2.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP2")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP2.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP2.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP3.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP3")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP3.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP3.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP4.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP4")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP4.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP4.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP5.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP5")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP5.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP5.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP6.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP6")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP6.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP6.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP7.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP7")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP7.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP7.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP8.getSelectedItem().toString().equals(read("com.sps.jvr.donderdagP8")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP8.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP8.setSelection(0);
                    System.out.println("Periode not found");
                }
                break;

            case 5:
                pos = 0;
                while(!spinnerP1.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP1")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP1.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP1.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP2.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP2")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP2.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP2.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP3.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP3")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP3.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP3.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP4.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP4")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP4.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP4.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP5.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP5")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP5.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP5.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP6.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP6")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP6.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP6.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP7.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP7")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP7.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP7.setSelection(0);
                    System.out.println("Periode not found");
                }

                pos = 0;
                while(!spinnerP8.getSelectedItem().toString().equals(read("com.sps.jvr.vrydagP8")) && pos <= maxPos){
                    pos++;
                    if (pos <= maxPos) {
                        spinnerP8.setSelection(pos);
                    }
                }
                if(pos > maxPos){
                    spinnerP8.setSelection(0);
                    System.out.println("Periode not found");
                }
                break;
        }*/

    }

    public void onTerugClick(View view){
        dayEditing --;

        if (dayEditing == 0){
            gotoActivity(LeerderConfig1.class);
        }else {
            storeSpinners(dayEditing + 1);
            setDayTxt(dayEditing);
            setSpinners(dayEditing);
        }
    }
    
    public void  onBackPressed () {
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void setDayTxt(int dag){
        TextView txtDag = (TextView) findViewById(R.id.txtDag);
        switch (dag){
            case 1:
                txtDag.setText("Maandag");
                break;
            case 2:
                txtDag.setText("Dinsdag");
                break;
            case 3:
                txtDag.setText("Woensdag");
                break;
            case 4:
                txtDag.setText("Donderdag");
                break;
            case 5:
                txtDag.setText("Vrydag");
                break;
        }
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void write(String key, String val){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public String read(String key){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sps.jvr.info", this.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public void onMissingClick(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Ontbreking")
                .setMessage("Is daar 'n vak nie hier nie? Kort daar 'n naskoolse aktiwiteit? Is die periodes verkeerd?\nAs daar enigiets fout is, laat weet my onmiddelik!")
                .setPositiveButton("Gee Terugvoer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LeerderConfig2.this, Feedback.class);
                        startActivity(intent);
                        //purposefully not finish()ing so that user can return to this activity as it was
                    }
                })
                .setNegativeButton("Nvm", null)
                .show();
    }

    public void onRandomiseClick(View view) {
        for (int i = 1; i <= 8; i++){
            Spinner spin = (Spinner) findViewById(getResources().getIdentifier("spinnerP"+i, "id", getPackageName()));
            spin.setSelection(JanTools.randInt(1, spin.getAdapter().getCount()-1));
        }
    }
}

