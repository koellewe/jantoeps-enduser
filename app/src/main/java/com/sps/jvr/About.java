package com.sps.jvr;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sps.jvr.utils.JanTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class About extends Activity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.exListView);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        setWeergawe();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ScrollView sv = (ScrollView) findViewById(R.id.scrollView);
                sv.scrollTo(0, 0);
                System.out.println("yes");
            }
        }, 200);
    }

    private void setWeergawe(){

        TextView weergaweView = (TextView) findViewById(R.id.txtWeergawe);

        if (JanTools.read("version", "false")){
            try{
                PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                weergaweView.setText(pinfo.versionName);
            }catch(Exception e){
                e.printStackTrace();
                weergaweView.setVisibility(View.GONE);
            }
        }else{

            int latestVersion = Integer.parseInt(JanTools.read("version"));
            int currentVersion = -1;
            String currentVersionText = "";
            try{
                PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                currentVersion = pinfo.versionCode;
                currentVersionText = pinfo.versionName;
            }catch(Exception e){
                e.printStackTrace();
                weergaweView.setVisibility(View.GONE);
            }

            String out = "Weergawe: " + currentVersionText;
            if (currentVersion < latestVersion){
                out += " (verouderd)";
            }else if (currentVersion == latestVersion){
                out += " (nuutste)";
            }else{
                out += " (dev)";
            }

            weergaweView.setText(out);

        }

    }

    public void prepareListData(){
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding child data
        /*listDataHeader.add("");
        List<String> l0 = new ArrayList<>();
        l0.add("");*/

        //listDataChild.put(listDataHeader.get(0), l0); // Header, Child data

        String[] hulp = {
                "Die toeps sê daar is 'n opdatering, maar die Play store sê daar is nie.",
                "Nadat 'n opdatering vrygelaat is, moet Google eers die app scan vir virusse en so aan. Dit vat gewoonlik 12 ure. Wag net 'n ruk, dan hoort dit reg te wees.",
                "Herkonfigurasie werk nie.",
                "Restart jou foon nadat jy die knoppie gedruk het.",
                "Hoekom is die 10H funksie weg?",
                "Onprakties om dit elke jaar op te dateer.",
                "Die tye is 'n paar minute uit.",
                "Jou foon se tyd is verkeerd. Kyk na jou rooster se tye en stel dit reg wanneer die volgende klok lui.",
                "Die tye is baie verkeerd.",
                "Onthou dat daar tyd stellings is by die stellings. Die toeps kan baie dinge doen, maar dit gebeur dat daar partykeer iets voorval wat die toeps nie kan hanteer nie. Ek werk konstant aan nuwe algoritmes om spesiale gebeurtenisse te hanteer. Dit is baie moeite, so gee kans.",
                "Wat beteken die \"voor eerste klok lui\"?",
                "Voor skool en in pouses word daar 'n \"eerste klok\" gelui 5 minute voordat die periode regtig begin, sodat kinders solank begin loop na die klasse toe en nie enige akademiese tyd mors nie. \"Tweede klok\" lui wanneer die periode regtig begin. Die funksie is daar vir akkuraatheid.",
                "Hoekom vervris die tye nie hulself nie?",
                "Dit vat 'n soliede hoeveelheid verwerkingskrag om die tye te vervris, en om dit in die agtergrond te doen, sal maak dat jou foon baie stadiger werk. Hoeveel moeite is dit regtig om net self die knoppie te druk?",
                "Ek het 'n idee!",
                "Druk bo om vir my terugvoer te gee!",
                "Die periodes is verkeerd.",
                "Onthou dat daar periode stellings is by die stellings. Jy het hulle waarskynlik verkeerd gekonfigureer. As jy dink dit is jou skuld, moet jy alles oor herkonfigureer. Gaan kyk by stellings.",
                "Hoekom vibreer my foon?",
                "Die toeps het 'n funksie wat maak dat jou foon 5 minute voor 'n periode eindig, vibreer. Dit werk nog nie 100% nie. Jy kan dit afskakel by jou stellings.",
                "My foon vibreer nie elke periode nie.",
                "Jammerte. Soos ek sê, daardie funksie werk nog nie, so gee kans.",
                "Sink fout/flater.",
                "Issue met die sinkronisasie met die bediener. Hou aan druk op \"Sinkroniseer\" totdat dit weg is. En maak natuurlik seker jy het internet toegang.",
                "Ek hou nie van advertensies nie",
                "Desperate times... Wees net bly die toeps is gratis.",
                "Wat is \"Eksamen Mode\"?",
                "Wanneer dit eksamens is, gaan die toeps in eksamen mode in, omdat dit nie regtig 'n gebruik het in daai tydperk nie. Op die oomblik doen eksamen mode min. Dit is net daar om jou nie te pla nie.",
                "Wat is \"Vakansie Mode\"?",
                "Wanneer dit vakansie is, het hierdie toeps geen nut nie. Die vakansie banier is net daar om jou nie te laat dink aan skool nie.",
                "Hoe werk die laatbus ding?",
                "Die laatbus herinneraar sal jou sê om jou naam op die lys te skryf, indien jy in die toeps in gaan tydens eerste pouse. Dit kan nog nie notifikasies stuur nie.",
                "Hoeveel data vat die toeps?",
                "Die toeps kyk elke oggend vir bywerkings. Dit vat so 50 bytes. As jy dit net een keer 'n dag doen (soos aanbeveel), sal dit omtrent 13 kilobytes in 'n jaar vat.\nDan is daar advertensies. Een advertensie kan enigiets van 500 bytes tot 50 kilobytes vat. Dit hang ook af van hoeveel keer per dag jy die toeps gebruik. Twee keer per periode sal 'n totaal van omtrent 10 megabytes per jaar verg.\nDit wil sê so min jy kon net sowel nie eers gevra het nie.",
                "Hoekom nie iPhones nie?",
                "\"Google leaves the responsibility of what apps users install and where users install them from to the user's discretion. Microsoft became the biggest operating system in the world with this philosophy, not by forcing users to use the windows app store to download and install programs. However the difference between microsoft and google is that android users never have to touch a single google service if they don't want to. This is more usually left at the device manufacturer's discretion, and many manufacturers and users alike WANT google services despite not being forced to use them. Users that do not wish to have google services can simply find a manufacturer shipping a device configured to suit their needs. For advanced users on both platforms, custom firmware configurations are an option, although it is much more accepted and possible with android OEMs. Apple locks down iOS as tight as possible for security purposes, automatically voiding warranties of anyone who dare desire full control over their device. This security is false because developers with the intent can, have, and will, find exploits to give them full control of these mobile devices. Making this increasingly difficult only proves further that Apple has a sense of entitlement when it comes to control of the devices that users pay for and own. I suppose this can be expected of a company that attempts to patent a rectangle with smooth corners.\"",
                "Daar is fout met die sink.",
                "As die toeps wys dat dit eksamens is en dit is nie (of soortgelyk), is dit moontlik dat die wolkbediener nog nie met regte lewe opgedateer is nie. As jy glo dat die gesinkroniseerde inligting verkeerd is, kan jy dit af sit by stellings. Dit sal dan terugval na die verstekwaardes. Wees net seker om dit weer aan te skakel die volgende dag.",
                "Waar is die \"Dev Stellings\"?",
                "Druk 5 keer op die wifi ikoon by Stellings.",
                "Ek kan nie enigiets doen by terugvoer nie.",
                "Restart die toeps.",
                "Hoe restart ek die toeps?",
                "Jy moet dit heeltemal uit die geheue uit haal. Nie net toe- en oopmaak nie. Gaan na \"recent apps\" en swipe JanToeps weg, of druk op \"clear all\".\nAs jy dit nie kan doen nie of net te lui is, kan jy die toeps dwing om te crash by die DEV stellings.",
                "Wat is \"Catchphrase Mode\"?",
                "Dit verander vakke se name na iets lekkerders toe. Op die oomblik in ontwikkeling.",
                "Ek het daai ding gemis by naskoolse aktiwiteite.",
                "Jy kan dit herstel in die dev stellings. Good luck.",
                "Wat is \"DNS\"?",
                "Kort vir Do-Not-Show. Wanneer jy click op 'moenie weer hierdie wys nie' aktiveer jy 'n DNS op daai boodskap. Dit sal nooit weer gewys word nie. Jy kan alle DNS'e herstel in die Dev stellings as jy wil.",
                "Wat is die punt van die lewe?",
                "Goeie vraag."
        };

        for (int listPos = 0; listPos <= hulp.length -2; listPos += 2){
            listDataHeader.add(hulp[listPos]);
            List<String> tmpList = new ArrayList<>();
            tmpList.add(hulp[listPos+1]);
            listDataChild.put(listDataHeader.get(listPos /2), tmpList);
        }
    }

    public void onTerugClick(View view) {
         gotoActivity(Settings.class);
    }
    
    public void  onBackPressed () {
        onTerugClick(findViewById(R.id.btnTerug));
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onEmailClick(View view){
        Intent intent = new Intent(this, Feedback.class);
        startActivity(intent);  //to return
    }
}
